﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour {
    public Sprite[] HeartSprites;

    public Image HeartUI;

    private Player1 player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player1>();
    }

    void Update()
    {
        HeartUI.sprite = HeartSprites[player.curHealth];
    }
}
