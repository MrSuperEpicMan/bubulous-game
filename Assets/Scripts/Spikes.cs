﻿using UnityEngine;
using System.Collections;

public class Spikes : MonoBehaviour
{
    private Player1 player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player1>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            player.Damage(3);

            StartCoroutine(player.Knockback(0.02f, 80f, new Vector3(player.transform.position.x, player.transform.position.y + 0.5f, player.transform.position.z)));
        }
    }
}