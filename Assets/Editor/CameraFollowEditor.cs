﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(_CameraFollow))]
public class CameraFollowEditor : Editor {
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        _CameraFollow cf = (_CameraFollow)target;
        if (GUILayout.Button("Set Min Cam Pos"))
        {
            cf.SetMinCamPosition();
        }

        if (GUILayout.Button("Set Max Cam Pos"))
        {
            cf.SetMaxCamPosition();
        }
    }
}
